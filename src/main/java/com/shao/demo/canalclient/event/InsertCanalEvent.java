package com.shao.demo.canalclient.event;

import com.alibaba.otter.canal.protocol.CanalEntry;

/**
 * 插入事件
 * @author zhiqi.shao
 * @Date 2018/6/6 14:24
 */
public class InsertCanalEvent extends CanalEvent {

    public InsertCanalEvent(CanalEntry.Entry source) {
        super(source);
    }
}
