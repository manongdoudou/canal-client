package com.shao.demo.canalclient.client;

import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.common.utils.AddressUtils;
import com.google.common.collect.Lists;
import com.shao.demo.canalclient.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.InetSocketAddress;

/**
 * @author zhiqi.shao
 * @Date 2018/6/6 10:10
 */
@Component
public class CanalClient implements DisposableBean {
    private static final Logger logger = LoggerFactory.getLogger(CanalClient.class);
    private CanalConnector canalConnector;

    @Autowired
    AppConfig appConfig;


    @Bean
    public CanalConnector getCanalConnector() {
        if(appConfig==null){
            throw new RuntimeException("ERROR ,appConif is null");
        }
        String ip;
        if (StringUtils.isEmpty(appConfig.getIp())) {
            ip = AddressUtils.getHostIp();
        } else {
            ip = appConfig.getIp();
        }
        // 创建链接
        InetSocketAddress inetSocketAddress = new InetSocketAddress(ip, Integer.valueOf(appConfig.getPort()));
        canalConnector = CanalConnectors.newClusterConnector(Lists.newArrayList(inetSocketAddress), appConfig.getDestination(), appConfig.getUsername(), appConfig.getPassport());
        // 链接canal
        canalConnector.connect();
        // 指定filter，格式 {database}.{table}，这里不做过滤，过滤操作留给用户
        canalConnector.subscribe();
        // 回滚寻找上次中断的位置
        canalConnector.rollback();
        logger.info("canal客户端启动成功");
        return canalConnector;
    }

    /**
     * 当bean销毁时断开canal的链接
     */
    @Override
    public void destroy() {
        if (canalConnector != null) {
            canalConnector.disconnect();
        }
    }
}